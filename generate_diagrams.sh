#!/usr/bin/env bash

find . -iname "*.pu" -exec echo "Generate {}" \; -exec plantuml -Tpng {} \;
