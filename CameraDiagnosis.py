#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""This module try to determine the camera problem root cause."""

import argparse
import logging
import os.path
import subprocess
import sys
import time

import qi
import vision_definitions as vd


def to_camera_name(index):
    """Get Camera name from Naoqi CameraID"""
    return {
        vd.kTopCamera: 'CameraTop',
        vd.kBottomCamera: 'CameraBottom',
        vd.kStereoCamera: 'CameraStereo'
        }.get(index, 'None')


def to_camera_device(index):
    """Get Camera device file from Naoqi CameraID"""
    return {
        vd.kTopCamera: '/dev/video-top',
        vd.kBottomCamera: '/dev/video-bottom',
        vd.kStereoCamera: '/dev/video-stereo'
        }.get(index, 'None')


def to_camera_class_name(index):
    """Get Camera class/name from Naoqi CameraID"""
    return {
        vd.kTopCamera: 'LI-OV5640',
        vd.kBottomCamera: 'LI-OV5640',
        vd.kStereoCamera: 'OV580 STEREO'
        }.get(index, 'None')


def check_naoqi_camera(session, index):
    """Verify if Camera working using Naoqi"""
    check_naoqi_camera_index(session, index)
    try:
        try_naoqi_stream(session, index)
    except RuntimeError:
        try:
            try_naoqi_reset(session, index)
            try_naoqi_stream(session, index)
        except RuntimeError:
            try:
                try_i2c_reset(session, index)
                check_usb_camera(index)
                try_naoqi_stream(session, index)
            except RuntimeError:
                LOGGER.error('%s: Can\'t stream even after hardware reset!',
                             to_camera_name(index))
                LOGGER.fatal('PLEASE CHECK THE INTEGRITY OF %s!',
                             to_camera_name(index).upper())
                raise


def check_naoqi_camera_index(session, index):
    """Verify if ALVideoDevice know camera"""
    LOGGER.debug('Check %s via Naoqi...', to_camera_name(index))
    try:
        video_device = session.service('ALVideoDevice')
        cameras = video_device.getCameraIndexes()
        cameras.index(index)
        LOGGER.debug('%s: FW loaded before Naoqi OK.', to_camera_name(index))
    except ValueError:
        LOGGER.fatal('%s: Push FW too long!', to_camera_name(index))
        LOGGER.fatal('PLEASE TRY TO RESTART NAOQI')
        raise RuntimeError("Push FW too long")


def try_naoqi_stream(session, index):
    """Verify if GetImageRemote is working"""
    if index == vd.kTopCamera or index == vd.kBottomCamera:
        resolution = vd.kVGA
    else:
        resolution = vd.k720px2
    video_device = session.service('ALVideoDevice')
    name = video_device.subscribeCamera('diagnosis',
                                        index,
                                        resolution,
                                        vd.kYUV422ColorSpace,
                                        15)
    alimage = video_device.getImageRemote(name)
    video_device.unsubscribe(name)
    if alimage is None:
        LOGGER.error('%s: Stream not working!', to_camera_name(index))
        raise RuntimeError('Can\'t stream using Naoqi')
    else:
        LOGGER.debug('%s: Streaming using Naoqi OK', to_camera_name(index))


def try_naoqi_reset(session, index):
    """Try to reset camera using ALVideoDevice"""
    LOGGER.warning('%s: try to reset using naoqi', to_camera_name(index))
    video_device = session.service('ALVideoDevice')
    video_device.resetCamera(index)


def try_i2c_reset(session, index):
    """Try to reset camera using CGOS/I2C"""
    video_device = session.service('ALVideoDevice')
    video_device.closeCamera(index)
    LOGGER.warning('%s: try to reset camera via i2c', to_camera_name(index))
    subprocess.check_call(['/usr/libexec/reset-cameras.sh', 'toggle'])
    # Time for CX3 to reload their camera firmware...
    time.sleep(5)


def main(session):
    """Base function to check all robot cameras"""
    clean_naoqi_subscribers(session)
    success = 0
    for index in [vd.kTopCamera, vd.kBottomCamera, vd.kStereoCamera]:
        try:
            check_usb_camera(index)
            check_naoqi_camera(session, index)
            LOGGER.info('%s: OK', to_camera_name(index))
        except RuntimeError:
            LOGGER.fatal('%s: NOK', to_camera_name(index))
            success = 1
    return success


def clean_naoqi_subscribers(session):
    """Remove all remaining subscribers from ALVideoDevice"""
    LOGGER.debug('Remove all subscribers from ALVideoDevice...')
    video_device = session.service('ALVideoDevice')
    for sub in video_device.getSubscribers():
        LOGGER.warning('Remove subscriber: %s', sub)
        video_device.unsubscribe(sub)


def check_usb_camera(index):
    """Check if camera is correctly detected by the kernel"""
    LOGGER.debug('Check %s...', to_camera_device(index))
    real_device_file = check_symlink(index)
    check_device_class_name(real_device_file, to_camera_class_name(index))


def check_symlink(index):
    """Check if device file symlink is present
    This will also test USB 3.0/2.0 since symlink are created only if correct
    mode"""
    path = to_camera_device(index)
    if os.path.exists(path):
        LOGGER.debug('%s: FW Correctly loaded', to_camera_name(index))
    else:
        LOGGER.fatal('%s: symlink %s not found!',
                     to_camera_name(index),
                     path)
        LOGGER.fatal('PLEASE CHECK THE INTEGRITY OF %s!',
                     to_camera_name(index).upper())
        raise RuntimeError('Device %s not Found !', path)
    return os.readlink(path)


def check_device_class_name(videox, name):
    """Check Camera device class/name"""
    try:
        subprocess.check_call(
            ['grep',
             '-q',
             name,
             '/sys/class/video4linux/{0}/name'.format(videox)],
            stderr=subprocess.STDOUT)
        LOGGER.debug('Check class name for device %s: OK', videox)
    except subprocess.CalledProcessError:
        LOGGER.fatal('%s: Wrong device class name !', videox)
        raise RuntimeError("Wrong device class name")


if __name__ == '__main__':
    # Setup Logger
    LOGGER = logging.getLogger(__name__)
    LOGGER.setLevel(logging.DEBUG)
    FORMATTER = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s')
    # FH = logging.FileHandler('camera_diagnosis.log')
    # FH.setLevel(logging.DEBUG)
    # FH.setFormatter(FORMATTER)
    CH = logging.StreamHandler()
    CH.setLevel(logging.DEBUG)
    CH.setFormatter(FORMATTER)
    # LOGGER.addHandler(FH)
    LOGGER.addHandler(CH)
    LOGGER.info("Version 1.1")

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address."
                        " On robot or Local Naoqi: use '127.0.0.1'.")
    PARSER.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    ARGS = PARSER.parse_args()
    SESSION = qi.Session()
    try:
        SESSION.connect("tcp://" + ARGS.ip + ":" + str(ARGS.port))
    except RuntimeError:
        print("Can't connect to Naoqi at ip \"" + ARGS.ip + "\" on port " +
              str(ARGS.port) + ".\n")
        print("Please check your script arguments." +
              " Run with -h option for help.")
        sys.exit(1)
    sys.exit(main(SESSION))
