# CameraDiagnosis Changelog
The latest version of this file can be found at the master branch of the
omnibus-gitlab repository.

1.1.0

- Disable file logger

1.0.0

- Initial Version
