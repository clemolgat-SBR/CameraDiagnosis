# Introduction
Here the production use case as a sequence diagram.  
![Protocol Sequence](protocol_sequence.png)

Current Issue: Sometime at step 6 we don't get images  

# Analyzer
Here the analyzer protocol as an activity diagram.  
![Protocol Activity](protocol_activity.png)

First deploy the tool on robot:
```sh
scp CameraDiagnosis.py nao@10.0.161.9:~
```

Then on robot run
```sh
python CameraDiagnosis.py
```

note: A log file *camera_diagnosis.log* will be created.

# Annexe
## Everything is good
Here an Example if all cameras are damaged.
```sh
Pepper [0] ~ $ python CameraDiagnosis.py 
2017-01-20 16:53:34,000 - INFO: Version 1.0
[W] 1484930367.270248 4130 qi.path.sdklayout: No Application was created, trying to deduce paths
2017-01-20 16:53:35,000 - INFO: CameraTop: OK
2017-01-20 16:53:36,000 - INFO: CameraBottom: OK
2017-01-20 16:53:37,000 - INFO: CameraStereo: OK
```

Here an Example if Stereo Camera is damaged.
```sh
Pepper [0] ~ $ python CameraDiagnosis.py 
2017-01-20 16:53:34,000 - INFO: Version 1.0
[W] 1484930367.270248 4130 qi.path.sdklayout: No Application was created, trying to deduce paths
2017-01-20 16:53:35,000 - INFO: CameraTop: OK
2017-01-20 16:53:36,000 - INFO: CameraBottom: OK
2017-01-20 16:53:36,768 - CRITICAL: CameraStereo: symlink /dev/video-stereo not found!
2017-01-20 16:53:36,768 - CRITICAL: PLEASE CHECK THE INTEGRITY OF CAMERASTEREO!
2017-01-20 16:53:36,769 - CRITICAL: CameraStereo: NOK
```

